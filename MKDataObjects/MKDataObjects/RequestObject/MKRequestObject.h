//
//  BeoModel.h
//  Beo
//
//  Created by Rolf Koczorek on 23.06.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequestOperationManager;

@interface MKRequestObject : NSObject

typedef enum{
    RequestTypeGET = 1,
    RequestTypePOST,
    RequestTypePUT,
    RequestTypePATCH,
    RequestTypeDELETE
}RequestType;

@property (nonatomic, strong) AFHTTPRequestOperationManager *httpRequestOperationManager;
@property (nonatomic, strong) NSMutableDictionary *httpHeaders;

-(void) setHttpHeaderField:(NSString *)headerField withValue:(NSString *)value;
-(void) performRequestWithType:(RequestType) type url: (NSURL*) url data: (NSDictionary*)data success:(void (^)(id))success error:(void (^)(NSError*))error uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)) uploadProgressBlock;

@end

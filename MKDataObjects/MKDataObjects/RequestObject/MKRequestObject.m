//
//  BeoModel.m
//  Beo
//
//  Created by Rolf Koczorek on 23.06.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "MKRequestObject.h"
#import "AFNetworking.h"

@implementation MKRequestObject

-(id) init {
    self = [super init];
    if(self) {
        self.httpHeaders = [[NSMutableDictionary alloc] init];
    }
    return self;
}


-(NSDictionary*) parametersFromData:(NSDictionary*) data
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    for(NSString *key in data) {
        id value = [data valueForKey:key];
        if(![value isKindOfClass:[NSData class]]) {
            [parameters setObject:value forKey:key];
        }
    }
    return parameters;
}

-(NSArray*) filesFromData:(NSDictionary*) data
{
    NSMutableArray *files = [[NSMutableArray alloc] init];
    for(NSString *key in data) {
        id value = [data valueForKey:key];
        if([value isKindOfClass:[NSData class]]) {
            [files addObject:value];
        }
    }
    return files;
}

-(void) performUploadRequestWithUrl:(NSURL*) url parameters:(NSDictionary*) parameters files:(NSArray*) files success:(void (^)(id))success error:(void (^)(NSError*))error uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)) uploadProgressBlock {
    NSString *urlString = [url absoluteString];
    AFHTTPRequestOperationManager *operationManager = self.httpRequestOperationManager;
    AFHTTPRequestOperation *op = [operationManager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        for(NSData *file in files) {
            [formData appendPartWithFileData:file name:@"file" fileName:@"image.jpg" mimeType:@"image/png"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if(success) { success(responseObject); };
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *errorObject) {
        if(error) { error(errorObject); }
    }];
    if(uploadProgressBlock) {
    [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        uploadProgressBlock(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite);
    }];
    }
}

-(void) performGetRequestWithUrl:(NSURL*) url parameters:(NSDictionary*) parameters success:(void (^)(id))success error:(void (^)(NSError*))error
{
    NSString *urlString = [url absoluteString];
    AFHTTPRequestOperationManager *operationManager = self.httpRequestOperationManager;
    [operationManager GET:urlString
            parameters:parameters
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                         if(success) { success(responseObject); };
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *errorObj) {
                          if(error) { error(errorObj); }
                      }
         ];
}

-(void) performPOSTRequestWithUrl:(NSURL*) url parameters:(NSDictionary*) parameters success:(void (^)(id))success error:(void (^)(NSError*))error
{
    NSString *urlString = [url absoluteString];
    AFHTTPRequestOperationManager *operationManager = self.httpRequestOperationManager;
    [operationManager POST:urlString
               parameters:parameters
                  success:^(AFHTTPRequestOperation *operation, id responseObject) {
                      if(success) { success(responseObject); };
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *errorObj) {
                      if(error) { error(errorObj); }
                  }
     ];
}

-(void) performRequestWithType:(RequestType) type url: (NSURL*) url data: (NSDictionary*)data success:(void (^)(id))success error:(void (^)(NSError*))error uploadProgressBlock:(void (^)(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)) uploadProgressBlock
{
    NSDictionary *parameters = [self parametersFromData:data];
    NSArray *files = [self filesFromData:data];
    
    
    if(files.count > 0) {
        [self performUploadRequestWithUrl:url parameters:parameters files:files success:success error:error uploadProgressBlock:uploadProgressBlock];
        return;
    }
    
    if(type== RequestTypeGET) {
            [self performGetRequestWithUrl:url parameters:parameters success:success error:error];
    }
    if(type == RequestTypePOST) {
        [self performPOSTRequestWithUrl:url parameters:parameters success:success error:error];
    }
}


-(AFHTTPRequestOperationManager*) httpRequestOperationManager
{
    AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
    [policy setAllowInvalidCertificates:YES];
    
    AFHTTPRequestOperationManager *operationManager = [AFHTTPRequestOperationManager manager];
    [operationManager setSecurityPolicy:policy];
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    operationManager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    for(NSString *key in self.httpHeaders) {
        NSString *value = [self.httpHeaders valueForKey:key];
        [operationManager.requestSerializer setValue:value forHTTPHeaderField:key];
    }
    return operationManager;
}


-(void) setHttpHeaderField:(NSString *)headerField withValue:(NSString *)value
{
    [self.httpHeaders setValue:value forKey:headerField];
}

@end

//
//  MKDataField.h
//  Huddle
//
//  Created by Rolf Koczorek on 06.08.14.
//  Copyright (c) 2014 Rolf Koczorek. All rights reserved.
//

#import "MKRequestObject.h"

@interface MKDataField : MKRequestObject <NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSMutableData *data;
@property (nonatomic) NSUInteger totalBytes;
@property (nonatomic) NSUInteger receivedBytes;
@property (nonatomic, copy) void (^progressBlock)(NSUInteger receivedBytes, NSUInteger totalBytes);
@property (nonatomic, copy) void (^successBlock)(void);
@property (nonatomic, copy) void (^errorBlock)(NSError *error);

@property (nonatomic, readonly) BOOL hasData;

-(void) pullWithProgressBlock:(void (^)(NSUInteger receivedBytes, NSUInteger totalBytes)) progressBlock successBlock:(void(^)(void)) successBlock errorBlock:(void(^)(NSError *error)) errorBlock;

@end

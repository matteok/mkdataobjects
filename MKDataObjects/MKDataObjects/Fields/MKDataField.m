//
//  MKDataField.m
//  Huddle
//
//  Created by Rolf Koczorek on 06.08.14.
//  Copyright (c) 2014 Rolf Koczorek. All rights reserved.
//

#import "MKDataField.h"

@implementation MKDataField

@synthesize hasData = _hasData;

-(void) pullWithProgressBlock:(void (^)(NSUInteger receivedBytes, NSUInteger totalBytes)) progressBlock successBlock:(void(^)(void)) successBlock errorBlock:(void(^)(NSError *error)) errorBlock
{
    self.successBlock = successBlock;
    self.errorBlock = errorBlock;
    self.progressBlock = progressBlock;
    NSURL *url = self.url;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    NSDictionary *dict = httpResponse.allHeaderFields;
    NSString *lengthString = [dict valueForKey:@"Content-Length"];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSNumber *length = [formatter numberFromString:lengthString];
    self.totalBytes = length.unsignedIntegerValue;
    self.data = [[NSMutableData alloc] initWithCapacity:self.totalBytes];
}
     
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.data appendData:data];
    self.receivedBytes += data.length;
    
    if(self.progressBlock) {
        self.progressBlock(self.receivedBytes, self.totalBytes);
    }
    // Actual progress is self.receivedBytes / self.totalBytes
}
 
 - (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    if(self.successBlock) {
        self.successBlock();
        self.successBlock = nil;
        self.progressBlock = nil;
        self.errorBlock = nil;
    }
    //imageView.image = [UIImage imageWithData:self.imageData];
}
 
 - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if(self.errorBlock) {
        self.errorBlock(error);
    }
    //handle error
}

-(BOOL) hasData
{
    return self.data != nil;
}

@end

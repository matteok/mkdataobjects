//
//  BeoModel.m
//  Beo
//
//  Created by Rolf Koczorek on 23.06.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "MKDataModel.h"
//#import "AFNetworking.h"
#import "MKModelList.h"
#import <objc/runtime.h>
//#import "ModelPool.h"
#import "MKDataField.h"

@implementation MKDataModel

static NSMutableDictionary* modelPool = nil;

@synthesize data = _data;
@synthesize apiURL = _apiURL;


-(id) init
{
    self = [super init];
    if(self) {
        [self construct];
    }
    return self;
}

-(id) initWithID:(NSInteger)id
{
    self = [super init];
    /*ModelPool *modelPool = [ModelPool sharedInstance];
    MKDataModel *model = [modelPool modelOfType:NSStringFromClass([self class]) withID:id];
    if(model) {
        return model;
    }
    [self construct];
    [modelPool storeModel:self];*/
    if(self) {
        [self construct];
    }
    return self;
}

-(MKDataModel*) modelWithID:(NSInteger)id
{
    /*ModelPool *modelPool = [ModelPool sharedInstance];
    MKDataModel *model = [modelPool modelOfType:NSStringFromClass([self class]) withID:id];
    if(model) {
        //NSLog(@"retrieved model from pool");
        return model;
    }
    self.id = id;
    [modelPool storeModel:self];*/
    //NSLog(@"did not retrieve model from pool");
    self.id = id;
    return self;
}

-(void) construct
{
   /* NSArray *keys = [self dataKeys];
    for(NSString *key in keys) {
        if([self respondsToSelector:NSSelectorFromString(key)]) {
            NSString *dataKey = [NSString stringWithFormat:@"%@Data", key];
            if([self respondsToSelector:NSSelectorFromString(dataKey)]) {
                MKDataField *dataField = [[MKDataField alloc] init];
                [self setValue:dataField forKey:dataKey];
            }
        }
    }*/
}

//recursively fills a model list and fires a callback
-(void) fillModelList: (MKModelList*) modelList withIDs:(NSArray*) ids  index: (NSInteger) index key:(NSString*) key callback: (void(^)(void)) callback
{
    if(ids.count > index) {
        MKDataModel *dataModel = [self foreignModelForKey:key];
        if(dataModel) {
            dataModel.httpHeaders = self.httpHeaders;
            NSString *idString = [ids objectAtIndex:index];
            NSInteger id = [idString integerValue];
            dataModel = [dataModel modelWithID:id];
            if(!dataModel.didPull) {
                [dataModel pull:^{
                    [modelList addModel:dataModel];
                    [self fillModelList:modelList withIDs:ids index:(index+1) key:key callback:callback];
                }error:^(NSError *_error) {
                    [self fillModelList:modelList withIDs:ids index:(index+1) key:key callback:callback];
                }];
            }
            else {
                [modelList addModel:dataModel];
                [self fillModelList:modelList withIDs:ids index:(index+1) key:key callback:callback];
            }
        }
        else {
            [self fillModelList:modelList withIDs:ids index:(index+1) key:key callback:callback];
        }
    }
    else {
        callback();
    }
}

//check if model has property [key]Extension
-(BOOL) hasKeyExtension:(NSString*)key extension:(NSString*) extension
{
    NSString *externalKey = [NSString stringWithFormat:@"%@%@", key, extension];
    return [self respondsToSelector:NSSelectorFromString(externalKey)];
}

-(void) setForeignModelForKey:(NSString*) key andValue:(id) value complete:(void(^)(void)) complete
{
    NSString *modelKey = [NSString stringWithFormat:@"%@Model", key];
    
    MKDataModel *foreignModel = [self foreignModelForKey:key];
    if(foreignModel) {
        foreignModel = [foreignModel modelWithID:[value integerValue]];
        foreignModel.httpHeaders = self.httpHeaders;
        if(!foreignModel.didPull) {
            [foreignModel pull:^{
                [self setValue:foreignModel forKey:modelKey];
                if(complete) complete();
                //[self processData:data index:(index+1) callback:callback];
            } error:^(NSError *error) {
                if(complete) complete();
            }];
        }
        else {
            [self setValue:foreignModel forKey:modelKey];
            if(complete) complete();
        }
    }
    else {
        if(complete) complete();
    }
}

//set a model list for a key
-(void) setModelListForKey:(NSString*) key withValue:(id) value complete: (void(^)(void)) complete
{
    //if model has property [key]List create ModelList set and pull
    NSString *listKey = [NSString stringWithFormat:@"%@List", key];
    MKModelList *modelList = [[MKModelList alloc] init];
    NSArray *ids = value;
    if(ids) {
        if(ids.count > 0) {
            [self fillModelList:modelList withIDs:ids index:0 key:key callback:^{
                [self setValue:modelList forKey:listKey];
                if(complete) complete();
            }];
        }
        else {
            if(complete) complete();
        }
    }
    else {
        if(complete) complete();
    }
}

-(void) setDataFieldForKey:(NSString*) key withValue:(id) value complete: (void(^)(void)) complete
{
    NSString *dataKey = [NSString stringWithFormat:@"%@Data", key];
    MKDataField *dataField = [[MKDataField alloc] init];
    NSString *urlString = (NSString*) value;
    dataField.url = [NSURL URLWithString:urlString];
    [self setValue:dataField forKey:dataKey];
    complete();
}

//recursively iterate over keys in data and set values if self has key. if property [key]Model is defined an MKDataModel will be instantiated and set for [key]Model
-(void) processData:(NSDictionary*) data index: (NSInteger) index callback:(void (^)())callback
{
    if(data.allKeys.count > index) {
        NSString *key = [[data allKeys] objectAtIndex:index];
        if([self respondsToSelector:NSSelectorFromString(key)]) {
            id value = [data objectForKey:key];
            [self setValue:value forKey:key];
            
            
            id me = self;
            
            //check if property [key]Model exists and set value if necessary
            if([self hasKeyExtension:key extension:@"Model"]) {
                [self setForeignModelForKey:key andValue:value complete:^{
                    [me processData:data index:(index+1) callback:callback];
                }];
            }
            
            //check if property [key]Lists exists and set value if necessary
            else if([self hasKeyExtension:key extension:@"List"]) {
                [self setModelListForKey:key withValue:value complete:^{
                    [me processData:data index:(index+1) callback:callback];
                }];
            }
            
            //check if property [key]Data exists and set value if necessary
            else if([self hasKeyExtension:key extension:@"Data"]) {
                [self setDataFieldForKey:key withValue:value complete:^{
                    [me processData:data index:(index+1) callback:callback];
                }];
            }
            
            else {
                [self processData:data index:(index+1) callback:callback];
            }
        }
        else {
             [self processData:data index:(index+1) callback:callback];
        }
    }
    else {
        //NSLog(@"last process data");
        callback();
    }
    
}

-(BOOL) dataBelongsToMe: (NSDictionary*) data
{
    if(data) {
        NSString *type = [data objectForKey:@"type"];
        NSDictionary *dataObj = [data objectForKey:@"data"];
        if(data && type) {
            if([type isEqualToString:self.typeName]) {
                NSString *idString = [dataObj objectForKey:@"id"];
                NSInteger id = [idString integerValue];
                if(id==self.id) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

//setters

-(void) setData:(NSDictionary *)data callback:(void (^)())callback
{
    _data = data;
    if(self.data.allKeys.count > 0) {
        [self processData:self.data index:0 callback:^{
            callback();
        }];
    }
}

//getters
-(NSArray*) dataKeys
{
    return @[@"id"];
}

-(MKDataModel*) foreignModelForKey:(NSString *)key
{
    return nil;
}



//actions

-(void) pull:(void (^)())success error:(void (^)(NSError *))error
{
    self.didPull = YES;
    //NSLog(@"pull in mkdatamodel with id %i", self.id);
    if(!self.id || self.id == 0 ) {
        return;
    }
    
    
    /*[self performRequestWithType:RequestTypeGET url:[self urlWithID] parameters:nil success:^(NSDictionary *result) {
       // NSLog(@"result from pulling in mkdatamodel");
        [self setData:result callback:^{
            success();
        }];
    }error:^(NSError * _error) {
        NSLog(@"error pulling mkdatamodel: %@", _error);
        error(_error);
    }];*/
    [self performRequestWithType:RequestTypeGET url:[self urlWithID] data:nil success:^(id result) {
        [self setData:result callback:^{
            success();
        }];
    }error:^(NSError *_error) {
        error(_error);
    }uploadProgressBlock:nil];
}

-(void)persist:(void (^)())success error:(void (^)(NSError *))error
{

    NSDictionary *data = [self toDictionary];
        /*[self performRequestWithType:RequestTypePOST url:self.apiURL parameters:data success:^(id result){
            success();
        }error:^(NSError *_error) {
            error(_error);
        }];*/
    [self performRequestWithType:RequestTypePOST url:self.apiURL data:data success:success error:error uploadProgressBlock:nil];
}

-(NSData*) dataForKey:(NSString*) key {
    NSString *dataKey = [NSString stringWithFormat:@"%@Data", key];
    if([self respondsToSelector:NSSelectorFromString(dataKey)]) {
        MKDataField *dataField = (MKDataField*) [self valueForKey:dataKey];
        if(dataField) {
            return dataField.data;
        }
    }
    return nil;
}

-(NSDictionary*) toDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    for(NSString *key in self.dataKeys) {
        if([self respondsToSelector:NSSelectorFromString(key)]) {
            NSData *data = [self dataForKey:key];
            if(data) {
               [dictionary setValue:data forKey:key];
            }
            else {
                [dictionary setValue:[self valueForKey:key] forKey:key];
            }
        }
    }
    return dictionary;
}

-(NSURL*) urlWithID
{
    NSString *apiUrlString = [self.apiURL absoluteString];
    NSString *urlString = [NSString stringWithFormat:@"%@%i/", apiUrlString, self.id];
    NSURL *url = [NSURL URLWithString:urlString];
    return url;
}


//MKModelList data source
-(MKDataModel*) modelListDataModel:(MKModelList *)modelList
{
    return nil;
}





@end

//
//  BeoModel.h
//  Beo
//
//  Created by Rolf Koczorek on 23.06.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKRequestObject.h"
#import "MKModelList.h"

@class MKModelList;

@interface MKDataModel : MKRequestObject



@property (nonatomic, strong) NSDictionary *data;

@property (nonatomic) NSInteger id;
@property (nonatomic, strong) NSURL *apiURL;
@property (nonatomic, strong) NSArray *dataKeys;
@property (nonatomic) BOOL didPull;
@property (nonatomic, readonly, strong) NSString *typeName;

-(id) initWithID:(NSInteger) id;
-(void) construct;
-(void) pull:(void(^)()) success error:(void(^)(NSError *error)) error;
-(void) setData:(NSDictionary *)data callback:(void(^)()) callback;
-(MKDataModel*) foreignModelForKey: (NSString*) key;
-(void) persist: (void(^)()) success error:(void(^)(NSError *error)) error;
-(NSDictionary*) toDictionary;
-(NSURL*) urlWithID;
//returns a static model or creates one
-(MKDataModel*) modelWithID: (NSInteger) id;
//determine wether or not a set of data belongs to a model
-(BOOL) dataBelongsToMe: (NSDictionary*) data;

@end

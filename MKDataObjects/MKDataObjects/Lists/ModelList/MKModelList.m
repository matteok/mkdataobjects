//
//  HuddleModel.m
//  Huddle
//
//  Created by Rolf Koczorek on 29.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "MKModelList.h"
#import "MKDataModel.h"

@implementation MKModelList

@synthesize data = _data;

//initializers
-(id) init
{
    self = [super init];
    if(self) {
        [self construct];
    }
    return self;
}

-(id) initWithApiURL:(NSURL *)apiURL
{
    self = [super init];
    if(self) {
        [self construct];
        self.apiURL = apiURL;
    }
    return self;
}

-(void) construct
{
    self.models = [[NSMutableArray alloc] init];
    
    //MKPubSub *pubSub = [MKPubSub sharedPubSub];
    //[pubSub subscribeChannel:@"push" object:self selector:@selector(dataPushed:)];
}

-(void) dataPushed: (NSDictionary*) data
{
    if(![self dataIsValid:data]) {
        return;
    }
    if([self pushDataConformsToType:data]) {
        if([self data:[data objectForKey:@"data"] conformsToDataConditions:[[self dataConditions] objectForKey:@"data"]]) {
            [self addModelWithData:[data objectForKey:@"data"]];
        }
    }
}

-(BOOL) dataIsValid: (NSDictionary*) data
{
    NSString *type = [data objectForKey:@"type"];
    NSDictionary *dataObj = [data objectForKey:@"data"];
    return dataObj && type;
}

-(BOOL) data:(NSDictionary*) data conformsToDataConditions: (NSDictionary*) dataConditions {
    
    if(!dataConditions || !data) {
        return YES;
    }
    
    int c = 0;
    for(NSString *key in dataConditions) {
        c++;
        id dataValue = [data objectForKey:key];
        id conditionValue = [dataConditions objectForKey:key];
        
        //check if any of the given conditions apply
        if([key isEqualToString:@"optional"]) {
            NSArray *optionals = (NSArray*) conditionValue;
            BOOL conforms = NO;
            for(NSDictionary *optional in optionals) {
                if([self data:data conformsToDataConditions:optional]) {
                    conforms = YES;
                }
            }
            if(!conforms) return NO;
            
        }
        else {
            if(!dataValue) {
                return NO;
            }
            NSString *conditionValueString = [NSString stringWithFormat:@"%@", conditionValue];
            NSString *dataValueString = [NSString stringWithFormat:@"%@", dataValue];
            if(![conditionValueString isEqualToString:dataValueString]) {
                return NO;
            }
            else {
            }
        }
        
        
        
    }
    
    return YES;
}

-(BOOL) pushDataConformsToDataConditions: (NSDictionary*) pushData {
    
    if(![self dataIsValid:pushData]) {
        return NO;
    }
    
    NSDictionary *pushConditions = [self dataConditions];
    NSDictionary *dataConditions = [pushConditions objectForKey:@"data"];
    NSDictionary *data = [pushData objectForKey:@"data"];
    if(!dataConditions || !data) {
        return NO;
    }
    for(NSString *conditionKey in dataConditions) {
        NSString *pushDataValue = [data objectForKey:conditionKey];
        if(!pushDataValue) {
            return NO;
        }
        
        NSString *conditionValue = [dataConditions objectForKey:conditionKey];
        
        NSString *val1 = [NSString stringWithFormat:@"%@", pushDataValue];
        NSString *val2 = [NSString stringWithFormat:@"%@", conditionValue];
        
        if(![val1 isEqualToString:val2]) {
            return NO;
        }
            
    }
    return YES;
}
    
-(BOOL) pushDataConformsToType: (NSDictionary*) pushData {
    
    if(![self dataIsValid:pushData]) {
        return NO;
    }
    
    NSDictionary *pushConditions = [self dataConditions];
    id allowedType = [pushConditions objectForKey:@"type"];
    if(allowedType) {
        NSArray *allowedTypes = @[];
        if([allowedType isKindOfClass:[NSString class]]) {
            allowedTypes = @[allowedType];
        }
        else if([allowedType isKindOfClass:[NSArray class]]) {
            allowedTypes = allowedType;
        }
        
        NSString *dataTypeName = [pushData objectForKey:@"type"];
        for(int i = 0; i < allowedTypes.count; i++) {
            
            NSString *typeName = [allowedTypes objectAtIndex:i];
            if(typeName) {
                if([typeName isEqualToString:dataTypeName]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

//actions

-(void) pull:(void (^)())success error:(void (^)(NSError *))error
{
    if(self.apiURL) {
        [self performRequestWithType:RequestTypeGET url:self.apiURL data:nil success:^(NSMutableArray *result) {
            if(result) {
                [self setData:result callback:^{
                    success();
                }];
            }
        }error:^(NSError *_error) {
            error(_error);
        }uploadProgressBlock:nil];
    }
    else {
        error(nil);
    }
}


-(void) addModel:(MKDataModel*) model
{
    for(MKDataModel *aModel in self.models) {
        if([aModel isMemberOfClass:[model class]]) {
            if(aModel.id == model.id) {
                return;
            }
        }
    }
    [self.models addObject:model];
    if([self.delegate respondsToSelector:@selector(modelListDidAddDataModel:)]) {
        [self.delegate modelListDidAddDataModel:model];
    }
    
}

-(void) addModelWithData:(NSDictionary *)data
{
    
    MKDataModel *model = [self dataModel];
    model.httpHeaders = self.httpHeaders;
    __weak MKDataModel *weakModel = model;
    [model setData:data callback:^{
        __strong typeof(weakModel) strongModel = weakModel;
        [self addModel:strongModel];
    }];
}


//getters
-(NSInteger) count
{
    return self.models.count;
}

-(MKDataModel*) objectAtIndex:(NSInteger)index
{
    if(self.models.count > index) {
        return [self.models objectAtIndex:index];
    }
    return nil;
}



-(void) processData:(NSArray*) data index: (NSInteger) index callback:(void (^)())callback
{
    if(self.data.count > index) {
        NSDictionary *dictionary = [data objectAtIndex:index];
        if(dictionary) {
            MKDataModel *model = [self dataModel];
            model.httpHeaders = self.httpHeaders;
            __weak typeof(self) weakSelf = self;
            __weak MKDataModel *weakModel = model;
            if(model) {
                
                [model setData:dictionary callback:^{
                    __strong typeof(self) strongSelf = weakSelf;
                    __strong typeof(weakModel) strongModel = weakModel;
                    
                    if(strongSelf && strongModel) {
                        [strongSelf addModel:strongModel];
                    }
                    [self processData:data index:(index+1) callback:callback];
                }];
            }
            else {
                [self processData:data index:(index+1) callback:callback];
            }
        }
        else {
            [self processData:data index:(index+1) callback:callback];
        }
    }
    else {
        callback();
    }
    
}

//setters

-(void) setData:(NSArray *)data callback:(void (^)())callback
{
    _data = data;
    if(self.data.count > 0) {
        [self processData:self.data index:0 callback:^{
            callback();
        }];
    }
}

-(MKDataModel*) dataModel
{
    return nil;
}

-(NSDictionary*) dataConditions
{
    return nil;
}


@end

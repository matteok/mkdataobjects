//
//  HuddleModel.h
//  Huddle
//
//  Created by Rolf Koczorek on 29.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "MKRequestObject.h"

@class MKDataModel;
@class MKModelList;


@protocol MKModelListDelegate <NSObject>

@optional
-(void) modelListDidAddDataModel:(MKDataModel*) model;

@end

@interface MKModelList : MKRequestObject

@property (nonatomic, weak) id<MKModelListDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *models;
@property (nonatomic, strong) NSURL *apiURL;
@property (nonatomic, strong) NSArray *data;
@property (nonatomic) NSInteger count;

-(id) initWithApiURL:(NSURL*) apiURL;
-(void) construct;
-(MKDataModel*) objectAtIndex:(NSInteger) index;
-(void) addModel:(MKDataModel*) model;
-(void) addModelWithData: (NSDictionary*) data;
-(void) pull:(void(^)()) success error:(void(^)(NSError *error)) error;
-(BOOL) pushDataConformsToDataConditions: (NSDictionary*) pushData;
-(BOOL) pushDataConformsToType: (NSDictionary*) pushData;
-(BOOL) data:(NSDictionary*) data conformsToDataConditions: (NSDictionary*) dataConditions;
-(NSDictionary*) dataConditions;

@end

//
//  HuddleModel.h
//  Huddle
//
//  Created by Rolf Koczorek on 29.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "MKModelList.h"

@class MKDataModel;
@class MKTypedModelList;


@interface MKTypedModelList : MKModelList


-(void) addModelWithData:(NSDictionary *)data andTypeName: (NSString*) typeName;
-(MKDataModel*) dataModelForType: (NSString*) type;

@end

//
//  HuddleModel.m
//  Huddle
//
//  Created by Rolf Koczorek on 29.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "MKTypedModelList.h"
#import "MKDataModel.h"

@implementation MKTypedModelList

//@synthesize count = _count;


//replaces addModelWithData
-(void) addModelWithData:(NSDictionary *)data andTypeName: (NSString*) typeName
{
    MKDataModel *model = [self dataModelForType:typeName];
    model.httpHeaders = self.httpHeaders;
    __weak MKDataModel *weakModel = model;
    [model setData:data callback:^{
        __strong typeof(weakModel) strongModel = weakModel;
       [self addModel:strongModel];
    }];
}

//override super method because typed list needs to implement addModelWithData:andTypeName:
-(void) dataPushed: (NSDictionary*) data
{
    if([self pushDataConformsToType:data]) {
        if([self data:[data objectForKey:@"data"] conformsToDataConditions:[[self dataConditions] objectForKey:@"data" ]]) {
            [self addModelWithData:[data objectForKey:@"data"] andTypeName:[data objectForKey:@"type"]];
        }
    }
}

-(void) processData:(NSArray*) data index: (NSInteger) index callback:(void (^)())callback
{
    //NSLog(@"process data in typed model list: %i", self.data.count);
    if(self.data.count > index) {
        NSDictionary *dictionary = [data objectAtIndex:index];
        if(dictionary) {
            NSString *type = [dictionary objectForKey:@"type"];
            NSDictionary *modelData = [dictionary objectForKey:@"data"];
            if(modelData && type) {
                MKDataModel *model = [self dataModelForType:type];
                model.httpHeaders = self.httpHeaders;
                __weak typeof(self) weakSelf = self;
                __weak MKDataModel *weakModel = model;
                if(model) {
                    [model setData:modelData callback:^{
                        __strong typeof(self) strongSelf = weakSelf;
                        __strong typeof(weakModel) strongModel = weakModel;
                        
                        if(strongSelf && strongModel) {
                            [strongSelf addModel:strongModel];
                        }
                        [self processData:data index:(index+1) callback:callback];
                    }];
                }
                else {
                    [self processData:data index:(index+1) callback:callback];
                }
            }
            else {
                [self processData:data index:(index+1) callback:callback];
            }
        }
        else {
            [self processData:data index:(index+1) callback:callback];
        }
    }
    else {
        callback();
    }
    
}

//subclasses need to implement this method in order to provide the specific models
-(MKDataModel*) dataModelForType:(NSString *)type
{
    return nil;
}

@end
